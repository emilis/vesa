import React                from 'react';


export default Object.assign(
    React.createContext( null ),
    {
        displayName:        'VesaContext',
    },
);
