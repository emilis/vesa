import PropTypes            from 'prop-types';
import React                from 'react';

import Context              from './Context';
import {
    ON_INIT,
    ON_SHUTDOWN,
}                           from './create-store';


export default class VesaMountStore extends React.PureComponent {

    static propTypes = {
        children:           PropTypes.node,
        store:              PropTypes.object.isRequired,
        /// ...restProps are passed to store.init()
    };

    constructor( props ){
        super( props );

        const {
            children,
            store,
            ...restProps
        } = props;
        store.init( restProps );
    }

    componentWillUnmount() {
        this.props.store.shutdown();
    }

    render() {
        const {
            store,
            children,
        } = this.props;

        return <Context.Provider value={ store }>
            { children }
        </Context.Provider>;
    }

}
