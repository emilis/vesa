import React                from 'react';

import Context              from './Context';


const DEFAULT_FILTER_FN =   () => null;


export default ( filterFn = DEFAULT_FILTER_FN ) => Child =>
    class VesaConnect extends React.PureComponent {

        static contextType =    Context;

        state = filterFn(
            this.context.getState(),
            this.props,
        );

        onChangeStore = fullState =>
            this.setState( filterFn(
                fullState,
                this.props,
            ));

        constructor( props, context ){
            super( props, context );

            context.onChange( this.onChangeStore );
        }

        componentWillUnmount() {
            this.context.offChange( this.onChangeStore );
        }

        render() {
            return <Child
                { ...this.props }
                { ...this.state }
                dispatch={ this.context.dispatch }
            />;
        }
    };
