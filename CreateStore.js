import PropTypes            from 'prop-types';
import React                from 'react';

import createStore          from './create-store';
import MountStore           from './MountStore';


export default class VesaCreateStore extends React.Component {

    static propTypes = {
        adapters:           PropTypes.object,
        children:           PropTypes.node,
        patchers:           PropTypes.object,
        /// ...restProps are passed to <MountStore />
    };

    store = createStore(
        this.props.patchers || {},
        this.props.adapters || {},
    );

    shouldComponentUpdate() {
        return false;
    }

    render() {
        const {
            adapters,
            children,
            patchers,
            ...props
        } = this.props;
        return <MountStore
            { ...props }
            children={ children }
            store={ this.store }
        />;
    }
}
