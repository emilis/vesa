import {
    ON_DISPATCH,
    ON_INIT,
    ON_SHUTDOWN,
}                                   from './constants';


export default ( patchers, uninitializedAdapters ) => {

    let adapters =                  [];
    let dispatching =               null;
    let fullState =                 {};
    const changeListeners =         new Set;

    /// API functions ----------------------------------------------------------

    const getState =                () => fullState;

    const offChange = fn =>
        changeListeners.delete( fn );

    const onChange = fn =>
        changeListeners.add( fn );

    const dispatch = ( eventName, payload ) => {

        if( dispatching ) {
            throw Error( `Cannot dispatch event ${ eventName } while dispatching event ${ dispatching }!` );
        }
        dispatching =               eventName;

        const newState =            {};

        const dispatchFor = ( executor, context ) =>
            ( executor[eventName] || executor[ON_DISPATCH] )
                ?.( payload, context );

        const waitFor = patcherName => {
            if( ! newState[patcherName] ) {
                const patch = dispatchFor( patchers[patcherName], {
                    eventName,
                    fullState,
                    payload,
                    state:          fullState[patcherName],
                    waitFor,
                });
                newState[patcherName] =
                    patch
                        ? { ...fullState[patcherName], ...patch }
                        : fullState[patcherName];
            }

            return newState[patcherName];
        };

        for( const patcherName in patchers ) {
            waitFor( patcherName );
        }

        const adapterContext = {
            dispatch,
            fullState,
            eventName,
            newState,
            payload,
            waitFor,
        };

        fullState =                 newState;
        dispatching =               null;

        for( const adapterName in adapters ) {
            dispatchFor( adapters[adapterName], adapterContext );
        }

        for( const fn of changeListeners ) {
            fn( fullState );
        }
    }

    const init = props =>
        dispatch( ON_INIT, props );

    const shutdown = () => {
        changeListeners.clear();
        dispatch( ON_SHUTDOWN );
    }

    /// main -------------------------------------------------------------------

    for( const patcherName in patchers ) {
        fullState[patcherName] =    {};
    }

    const store = {
        dispatch,
        getState,
        init,
        offChange,
        onChange,
        shutdown,
    };

    adapters = uninitializedAdapters.map( adapter =>
        adapter( store )
    );

    const knownEvents = [
        ...Object.values( adapters ),
        ...Object.values( patchers ),
    ].reduce(( acc, executor ) =>
        Object.keys( executor )
            .reduce(( acc, eventName ) =>
                acc.add( eventName ),
                acc,
            ),
        new Set,
    );

    for( const eventName of knownEvents ) {
        dispatch[eventName] =       dispatch.bind( null, eventName );
    }

    return store;
}
